<?php
/**
 * Created by PhpStorm.
 * User: cattong
 * Date: 2018-08-17
 * Time: 18:17
 */

return [
    'theme_name' => 'classic', //配置主题名称，与配置的主题文件夹一致
    'responsive' => false, //是否响应式界面：值为true,false; 可自动响应至pc,mobile,tablet
];